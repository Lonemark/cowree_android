package com.cowree.com.recyleview_drag_drop;

import com.cowree.com.Uploader.ProductImageDatas;
import com.cowree.com.utility.CapturedImage;

import java.util.List;

/**
 * Created by embed on 25/6/18.
 */

public interface OnCustomerListChangedListener {
    // use while post product
    void onNoteListChanged(List<CapturedImage> capturedImageList);
    // use while update product
    void onUpdateImagesListChanged(List<ProductImageDatas> productImageDatasList);
}
