package com.cowree.com.main.activity.products;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.cowree.com.R;
import com.cowree.com.main.activity.SplashActivity;
import com.cowree.com.utility.SessionManager;

public class LanguageSelectActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {
    private SessionManager mSessionManager;
    private Activity mActivity;
    private RadioButton rb_fr,rb_en;
    private RadioGroup rg_language;
    private String language_code;
    private int user_language_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_select);
        mActivity = LanguageSelectActivity.this;

        mSessionManager = new SessionManager(mActivity);

        // radio button for language
       /* rb_sb = (RadioButton) findViewById(R.id.rb_sb);
        rb_mg = (RadioButton) findViewById(R.id.rb_mg);*/
        rb_en = (RadioButton) findViewById(R.id.rb_en);
        rb_fr = (RadioButton) findViewById(R.id.rb_fr);

        rg_language = (RadioGroup) findViewById(R.id.rg_language);

        // intial checked button for current language
        currentLanguage(mSessionManager.getUserLanguageType());

        rg_language.setOnCheckedChangeListener(this);

        RelativeLayout rL_back_btn = (RelativeLayout) findViewById(R.id.rL_back_btn);

        rL_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });

        RelativeLayout rL_done = (RelativeLayout) findViewById(R.id.rL_apply);

        rL_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mSessionManager.setLanguageCode(language_code);
                mSessionManager.seUsertLanguageType(user_language_type);

                Intent i = new Intent(mActivity, SplashActivity.class);
                // set the new task and clear flags
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

            }

        });

    }

    public void currentLanguage(int language_type) {

        switch (language_type) {



            case 0:
                rb_en.setChecked(true);
                user_language_type=0;
                break;
            case 1:
                rb_fr.setChecked(true);
                user_language_type=1;
                break;

            default:
                rb_en.setChecked(true);


        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId) {

            case R.id.rb_en:
                language_code = getString(R.string.english_language_code);
                user_language_type=0;
                break;

            case R.id.rb_fr:
                language_code = getString(R.string.french_language_code);
                user_language_type=1;
                break;

        }

    }

}
